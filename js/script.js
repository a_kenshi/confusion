$(document).ready(function(){
    $('#mycarousel').carousel({interval:3000})
    $('#mycarousel').carousel('cycle')
});

/* Script Inner Login Modal */
$(document).on('click','#nsmoke',function(){
    $('#smoking').val("no-smoking");
})

$(document).on('click','#smoke',function(){
    $('#smoking').val("smoking");
})

$(document).on('click','#reserve',function(){
    var reserve = new Object();
    reserve.guest = $('input[name="inlineRadioOptions"]:checked').val();
    reserve.section = $('#smoking').val();
    reserve.date = $('#date').val();
    reserve.time = $('#time').val();
    alert(JSON.stringify(reserve));
})

/* Carousel Play-Pause */
$(document).on('click','#carouselButton',function(){
    if($('#carouselButton').children('span').hasClass('fa-pause')){
        $('#mycarousel').carousel('pause')
        $('#carouselButton').children('span').removeClass('fa-pause')
        $('#carouselButton').children('span').addClass('fa-play')
    }else{
        $('#mycarousel').carousel('cycle')
        $('#carouselButton').children('span').removeClass('fa-play')
        $('#carouselButton').children('span').addClass('fa-pause')
    }

})

/* Reserve Modal */
$(document).on('click','#reserveBtn',function () {
    $('#reserveTableModal').modal('show');
})

$(document).on('click','#reserveBtnDismiss,#reserveCancelBtn',function(){
    $('#reserveTableModal').modal('hide');
})

/* Login Modal */
$(document).on('click','#loginBtn',function () {
    $('#loginModal').modal('show');
})

$(document).on('click','#loginCloseBtn,#loginCancelBtn',function(){
    $('#loginModal').modal('hide');
})